# EvilCat Installer 1.0 b #

![EvilCatProject.jpg](https://bitbucket.org/repo/Aj9zMa/images/471367550-EvilCatProject.jpg)

EvilCat installer est un automate d'installation d'outils sur Raspberry pi.
Il installera automatiquement des outils connu comme Nmap, WireShark, Sqlmap, Wifite...
L’objectif est de faire du Raspberry un outil de hacking utile pour le Wardriving... et de le contrôlé SSH via un smartphone.

EvilCat installer va simplement être utile pour l'installation des outils nécessaire.

### Utilisation ###

sudo chmod +x ECI_1.0b.sh

./ECI_1.0b.sh

### Thanks ###

Z0MB13.D0LL